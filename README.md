# cval: C data type validator
---
## Instalation
    git clone git@github.com:MichelAyala/cval.git
    cd cval
    gcc -c -ggdb -I. --std=c11 cval.c -o cval.o
    ar rcs libcval.a cval.o
    chmod 0644 cval.h libcval.a
    mkdir -p /usr/local/include
    chmod 0755 /usr/local/include
    mv -f cval.h /usr/local/include
    mkdir -p /usr/local/lib
    chmod 0755 /usr/local/lib
    mv -f libcval.a /usr/local/lib
    cd ..
    rm -rf cval
---
## Usage
---
### Integers
    int num = getInteger();
    long lnum = getLongInteger();
    long long llnum = getLongLongInteger();

### Positive Integers
    unsigned unum = getPositiveInteger();
    unsigned long ulnum = getPositiveLongInteger();
    unsigned long long ullnum = getPositiveLongLongInteger();

### Negative Integers
    int nnum = getNegativeInteger();
    long nlnum = getNegativeLongInteger();
    long long nllnum = getNegativeLongLongInteger();

---

### Floats
    float num = getFloat();
    double lnum = getDouble();

### Positive Floats
    float pnum = getPositiveFloat();
    double plnum = getPositiveDouble();

### Negative Floats
    float nnum = getNegativeFloat();
    double nlnum = getNegativeDouble();

---
### Strings
    char *s = getString();
    // Stuff...
    free(s);
