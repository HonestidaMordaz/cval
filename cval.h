#ifndef __CVAL_H_
#define __CVAL_H_

// Integer validators
int getInteger(void);
long getLongInteger(void);
long long getLongLongInteger(void);

// Positive integer validators
unsigned getPositiveInteger(void);
unsigned long getPositiveLongInteger(void);
unsigned long long getPositiveLongLongInteger(void);

// Negative integer validators
int getNegativeInteger(void);
long getNegativeLongInteger(void);
long long getNegativeLongLongInteger(void);

// Float validators
float getFloat(void);
double getDouble(void);

// Positive float validators
float getPositiveFloat(void);
double getPositiveDouble(void);

// Negative float validators
float getNegativeFloat(void);
double getNegativeDouble(void);

// String validator (char *)
char *getString(void);

#endif