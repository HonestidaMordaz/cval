CC= gcc
CFLAGS= -g -W --std=c11
INC= -I.

cval.o: cval.h cval.c
	$(CC) $(CFLAGS) $(INC) -c cval.c

.PHONY: clean

clean:
	rm -f *.o