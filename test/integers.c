#include <stdio.h>
#include "cval.h"

int main(void)
{
	// Integer validators
	int num = getInteger();
	long lnum = getLongInteger();
	long long llnum = getLongLongInteger();

	// Positive integer validators
	unsigned unum = getPositiveInteger();
	unsigned long ulnum = getPositiveLongInteger();
	unsigned long long ullnum = getPositiveLongLongInteger();

	// Negative integer validators
	int nnum = getNegativeInteger();
	long nlnum = getNegativeLongInteger();
	long long nllnum = getNegativeLongLongInteger();

	printf("%d, %ld, %lld\n", num, lnum, llnum);
	printf("%u, %lu, %llu\n", unum, ulnum, ullnum);
	printf("%u, %lu, %llu\n", nnum, nlnum, nllnum);

	return 0;
}