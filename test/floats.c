#include <stdio.h>
#include "cval.h"

int main(void)
{
	// Float validators
	float num = getFloat();
	double lnum = getDouble();

	// Positive float validators
	float pnum = getPositiveFloat();
	double plnum = getPositiveDouble();

	// Negative float validators
	float nnum = getNegativeFloat();
	double nlnum = getNegativeDouble();

	printf("%f, %lf\n", num, lnum);
	printf("%f, %lf\n", pnum, plnum);
	printf("%f, %lf\n", nnum, nlnum);

	return 0;
}