#include <float.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cval.h"

/**
 * Integer validators
 */
int getInteger(void)
{
	int num;
	char badc;
	char *input;

	while (true) {
	        input = getString();
	
	        if (input == NULL)
			return INT_MAX;
	
	        if (sscanf(input, " %d %c", &num, &badc) == 1) {
			free(input);
			return num;
	        } else {
			free(input);
			printf("Error. Enter an integer: ");
	        }
	}
}

long getLongInteger(void)
{
	long int num;
	char badc;
	char *input;

	while (true) {
		input = getString();
			
		if (input == NULL)
		return LONG_MAX;
			
		if (sscanf(input, " %ld %c", &num, &badc) == 1) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a long integer: ");
		}
	}
}

long long getLongLongInteger(void)
{
	long long num;
	char badc;
	char *input;

	while (true) {
		input = getString();

		if (input == NULL)
			return LLONG_MAX;

		if (sscanf(input, " %lld %c", &num, &badc) == 1) {
			free(input);
			return num;
        	} else {
			free(input);
			printf("Error. Enter a long long integer: ");
		}
	}
}


/**
 * Positive Integer validators
 */
unsigned getPositiveInteger(void)
{
	unsigned num;
	char badc;
	char *input;

	while (true) {
		input = getString();

		if (input == NULL)
			return UINT_MAX;

		if (sscanf(input, " %u %c", &num, &badc) == 1 && num >= 0) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a positive number: ");
		}
	}
}

unsigned long getPositiveLongInteger(void)
{
	unsigned long num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return ULONG_MAX;
		
		if (sscanf(input, " %lu %c", &num, &badc) == 1 && num >= 0) {
			free(input);
			return num;
		} else {
				free(input);
			printf("Error. Enter a positive number: ");
		}
	}
}

unsigned long long getPositiveLongLongInteger(void)
{
	unsigned long long num;
	char badc;
	char *input;

	while (true) {
		input = getString();

		if (input == NULL)
			return ULLONG_MAX;

		if (sscanf(input, " %llu %c", &num, &badc) == 1 && num >= 0) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a positive number: ");
		}
	}
}

/**
 * Negative Integer validators
 */
int getNegativeInteger(void)
{
	int num;
	char badc;
	char *input;

	while (true) {
		input = getString();
	
		if (input == NULL)
			return INT_MIN;
	
		if (sscanf(input, " %d %c", &num, &badc) == 1 && num < 0) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a negative number: ");
		}
	}
}

long getNegativeLongInteger(void)
{
	long num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return LONG_MIN;
		
		if (sscanf(input, " %ld %c", &num, &badc) == 1 && num < 0) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a long negative number: ");
		}
	}
}

long long getNegativeLongLongInteger(void)
{
	long long num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
		return LLONG_MIN;
		
		if (sscanf(input, " %lld %c", &num, &badc) == 1 && num < 0) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a long negative number: ");
		}
	}
}

/**
 * Float validators
 */
float getFloat(void)
{
	float num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
	
		if (input == NULL)
			return FLT_MAX;
		
		if (sscanf(input, " %f %c", &num, &badc) == 1) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a floating number: ");
		}
	}
}

double getDouble(void)
{
	double num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return DBL_MAX;
		
		if (sscanf(input, " %lf %c", &num, &badc) == 1) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a double floating number: ");
		}
	}
}

/**
 * Positive Float validators
 */
float getPositiveFloat(void)
{
	float num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return FLT_MAX;
		
		if (sscanf(input, " %f %c", &num, &badc) == 1 && num >= 0.0f) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a positive floating number: ");
		}
	}
}

double getPositiveDouble(void)
{
	double num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return DBL_MAX;
		
		if (sscanf(input, " %lf %c", &num, &badc) == 1 && num >= 0.0f) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a positive double floating number: ");
		}
	}
}

/**
 * Negative Float validators
 */
float getNegativeFloat(void)
{
	float num;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return FLT_MIN;
		
		if (sscanf(input, " %f %c", &num, &badc) == 1 && num < 0.0f) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a negative floating number: ");
		}
	}
}

double getNegativeDouble(void)
{
	double num;
	char badc;
	char *input = getString();
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return DBL_MIN;
		
		if (sscanf(input, " %lf %c", &num, &badc) == 1 && num < 0.0f) {
			free(input);
			return num;
		} else {
			free(input);
			printf("Error. Enter a negative double floating number: ");
		}
	}
}

char getChar(void)
{
	char c;
	char badc;
	char *input;
	
	while (true) {
		input = getString();
		
		if (input == NULL)
			return CHAR_MAX;
		
		if (sscanf(input, " %c %c", &c, &badc) == 1) {
			free(input);
			return c1;
		} else {
			free(input);
			printf("Error. Enter a character ");
		}
	}
}

char *getString(void)
{
	char *buffer = NULL;
	unsigned int capacity = 0;
	unsigned int size = 0;
	
	// character read or EOF
	int c;
	
	while ((c = fgetc(stdin)) != '\n' && c != EOF) {
		
		if (size + 1 > capacity) {
		
			if (capacity == 0) {
				capacity = 32;
			} else if (capacity <= (UINT_MAX / 2)) {
				capacity *= 2;
			} else {
				free(buffer);
				return NULL;
			}
			
			// extend buffer's capacity
			char *temp = realloc(buffer, capacity * sizeof(char));
			
			if (temp == NULL) {
				free(buffer);
				return NULL;
			}
		
			buffer = temp;
		}
		
		buffer[size++] = c;
	}
	
	if (size == 0 && c == EOF)
		return NULL;
	
	char *minimal = malloc((size + 1) * sizeof(char));
	strncpy(minimal, buffer, size);
	free(buffer);
	
	minimal[size] = '\0';
	
	return minimal;
}
